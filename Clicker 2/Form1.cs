﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using System.Timers;
using System.IO.Ports;
using IronOcr;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;

namespace Clicker_2
{




    public partial class Form1 : Form
    {

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int BitBlt(IntPtr hDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);


        //Arduino
        System.Timers.Timer aTimer;
        SerialPort currentPort;
        private delegate void updateDelegate(string txt);

        
        private KeyHandler ghk;

        private long curTime;
        private long pickConditionDelay;
        private long pickConditionCurDelay;
        private long pickActionDelay;
        private long pickActionCurDelay;
        private Color tmpColor;
        private Color tmpColor2;
        private Point tmpPoint;
        private Point tmpPoint2;
        private Condition tmpDepend;
        public long freeze = 0;
        public long globalFreeze = 0;
        private long oldMilliseconds;
        Action a;
        Condition currentCondition;
        Action currentAction;
        List<Condition> conditions = Conditions.conditions;
        List<Action> actions = new List<Action>();
        public static Bitmap ReferenceApplicationBitmap;
        public static Bitmap ApplicationBitmap;
        public static Bitmap ApplicationBitmap2;
        private long ReferenceApplicationBitmapTime;

        public Form1()
        {
            InitializeComponent();


            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                 comboBox10.Items.Add(port);
            }
            comboBox10.SelectedIndex = comboBox10.Items.Count-1;




            ghk = new KeyHandler(Keys.Oemtilde, this);
            ghk.Register();

            Prepare();
            
            DirectoryInfo d = new DirectoryInfo(@Path.GetDirectoryName(System.IO.Path.GetDirectoryName(Application.ExecutablePath+"\\")));//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.sav"); //Getting Text files
            string str = "";
            foreach (FileInfo file in Files)
            {
                comboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(file.Name));
            }

            foreach (Process process in WindowHook.FindWindows())
            {
                if (!String.IsNullOrEmpty(process.MainWindowTitle))
                    comboBox11.Items.Add(process.MainWindowTitle);
            }

           

        }

        private void HandleHotkey()
        {
            // Do stuff...
            if (timer1.Enabled) Debug.Print("Stop"); else Debug.Print("Start");
            timer1.Enabled = !timer1.Enabled;
            button1.Enabled = !timer1.Enabled;
            button2.Enabled = timer1.Enabled;

        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Constants.WM_HOTKEY_MSG_ID)
                HandleHotkey();
            base.WndProc(ref m);
        }


        private void Prepare()
        {


            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;

            //Add column header
            listView1.Columns.Add("Comment", 90);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);


            /*
            //Add column header
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Comment", 90);
            */

            /////////////////             Actions

            listView2.View = View.Details;
            listView2.GridLines = true;
            listView2.FullRowSelect = true;

            //Add column header
            listView2.Columns.Add("Action", 65);
            listView2.Columns.Add("Location", 120);
            listView2.Columns.Add("Delay", 60);
            listView2.Columns.Add("Status", 50);
            listView2.Columns.Add("Left", 50);
            listView2.Columns[4].TextAlign = HorizontalAlignment.Right;
            listView2.Columns.Add("Comment", 100);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            button1.Enabled = false;
            button2.Enabled = true;
            WindowHook.sendKeystroke(comboBox11.Text, 'x');
            //Debug.Print(actions.ToString());
        }


        /*            //Add column header
            listView1.Columns.Add("Comment", 90);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);


            /*
            //Add column header
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Comment", 90);

            0-3
            1-4
            2-5
            3-1
            4-2
            5-0

            */
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem item = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0] : null;
            if (item != null)
            {
                comboBox2.Text = item.SubItems[3].Text;
                comboBox3.Text = item.SubItems[4].Text;
                comboBox4.Text = item.SubItems[5].Text;
                textBox1.Text = item.SubItems[0].Text;
                currentCondition = conditions[listView1.SelectedItems[0].Index];
                /*
                comboBox2.Text = item.SubItems[0].Text;
                comboBox3.Text = item.SubItems[1].Text;
                comboBox4.Text = item.SubItems[2].Text;
                textBox1.Text = item.SubItems[5].Text;
                currentCondition = conditions[listView1.SelectedItems[0].Index];
                */

                tmpPoint = currentCondition.point;
                tmpPoint2 = currentCondition.point2;
                tmpColor = currentCondition.color;
                tmpColor2 = currentCondition.color2;
                panel4.BackColor = currentCondition.color;
                checkBox2.Checked = currentCondition.async;
                checkBox3.Checked = currentCondition.enabled;
                loopActionsCheckBox.Checked = currentCondition.loopActions;

                comboBox8.Items.Clear();
                comboBox8.Items.Add("");
                comboBox8.SelectedIndex = 0;
                conditions.ForEach(delegate (Condition condition)
                {
                    comboBox8.Items.Add(condition.comment);
                });
                comboBox9.Text = currentCondition.logic.ToString();
                if (currentCondition.depend!=null) comboBox8.SelectedIndex = currentCondition.depend.id+1;
               actions = currentCondition.actions;
                RenderActions();

                comboBox5.Text = "";
                comboBox6.Text = "";
                comboBox7.Text = "";
                textBox2.Text = "";
            }
            else
            {
                comboBox2.Text = "";
                comboBox3.Text = "";
                comboBox3.Text = "";
                textBox1.Text = "";
            }
        }

        /*Fast timer to process conditions and actions*/
                private void timer1_Tick(object sender, EventArgs e)
        {

            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;


            ApplicationBitmap = CaptureApplication(WindowHook.HWND);
            ApplicationBitmap2 = (Bitmap) ApplicationBitmap.Clone();



            if (ReferenceApplicationBitmap == null)
            {
                ReferenceApplicationBitmap = (Bitmap)ApplicationBitmap.Clone();
            }

            //for motion detection
            if (milliseconds - ReferenceApplicationBitmapTime > 500)
            {
                if (ReferenceApplicationBitmap != null)
                {
                    ReferenceApplicationBitmap.Dispose();
                }
                if (ApplicationBitmap != null)
                    ReferenceApplicationBitmap = (Bitmap)ApplicationBitmap.Clone();
                ReferenceApplicationBitmapTime = milliseconds;
            }







            conditions.ForEach(delegate (Condition condition) {

                bool do_not_process = false;
                conditions.ForEach(delegate (Condition condition1) {
                    condition1.actions.ForEach(delegate (Action action)
                    {
                        if (action.curFreeze > 0)
                        {
                            do_not_process = true;
                            pickConditionCurDelay = pickConditionDelay - milliseconds;
                            action.curFreeze = action.curFreeze-(milliseconds - action.freezeOldMilliseconds);
                            action.freezeOldMilliseconds = milliseconds;
                        }
                    });
                });

                if (!do_not_process)
                    condition.Process();
            });


            if (serialPort1.IsOpen)
            {
                Conditions.toCom.ForEach(delegate (String command)
                {
                    serialPort1.WriteLine(command);
                });
            }
            Conditions.toCom.Clear();


            
            if (pictureBox1.Image != null)
            {
                pictureBox1.Image.Dispose();
            }

            pictureBox1.Image = (Bitmap) ApplicationBitmap2.Clone();



            if (pictureBox2.Image != null)
            {
                pictureBox2.Image.Dispose();
            }

            if (ReferenceApplicationBitmap != null)
            {
                pictureBox2.Image = (Bitmap)ReferenceApplicationBitmap.Clone();
            }




            ApplicationBitmap2.Dispose();
            ApplicationBitmap.Dispose();
            //oldMilliseconds = milliseconds;
        }


        /*Slow timer, to get values and render interface*/
        private void timer2_Tick(object sender, EventArgs e)
        {

            var c = GetColorAt(Cursor.Position);
            label1.Text = WindowHook.Cursor().ToString() + " " + ColorTranslator.ToHtml(c);
            panel1.BackColor = c;

            /*
            Bitmap r;
            if (currentCondition != null)
                r = currentCondition.Bmp;
            else
                r = GetRectAt(Cursor.Position);
            pictureBox1.Image = r;
            */

            //pictureBox1.Image = ApplicationBitmap;


            Rectangle rect = WindowHook.WindowRectangle();
            if (Cursor.Position.X > rect.X && Cursor.Position.X < rect.Width && Cursor.Position.Y > rect.Y && Cursor.Position.Y < rect.Height)
                label20.Text = "In";
            else
                label20.Text = "Out!";



            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            // Pick Condition point
            pickConditionCurDelay = pickConditionDelay - milliseconds;
            if (pickConditionCurDelay > 0)
            {
                label15.Text = Math.Round((pickConditionCurDelay / 1000.0f), 1).ToString();
                if (comboBox4.Text != WindowHook.Cursor().ToString() + ColorTranslator.ToHtml(c)) ;
                {
                    comboBox4.Text = WindowHook.Cursor().ToString() + ColorTranslator.ToHtml(c);
                    panel4.BackColor = c;
                }
                tmpPoint = WindowHook.Cursor();
                tmpColor = c;
            }
            else
            {
                if (label15.Text != "0")
                {
                    System.Media.SystemSounds.Beep.Play();
                    label15.Text = "0";
                }
            }

            // Pick Action point
            pickActionCurDelay = pickActionDelay - milliseconds;
            if (pickActionCurDelay > 0)
            {
                label14.Text = Math.Round((pickActionCurDelay / 1000.0f), 1).ToString();
                if (comboBox6.Text != WindowHook.Cursor().ToString())
                    comboBox6.Text = WindowHook.Cursor().ToString();
                tmpPoint = WindowHook.Cursor();
            }
            else
            {
                if (label14.Text != "0")
                {
                    System.Media.SystemSounds.Beep.Play();
                    label14.Text = "0";
                }
            }

            if (conditions != null) {
                var listEnumerator = conditions.GetEnumerator();
                for (var i = 0; listEnumerator.MoveNext() == true; i++)
                {
                    Condition currentItem = listEnumerator.Current;
                    
                    if (currentItem.enabled)
                        listView1.Items[i].SubItems[1].Text = currentItem.status;
                    else listView1.Items[i].SubItems[1].Text = "OFF";
                    listView1.Items[i].SubItems[2].Text = Math.Round(currentItem.cooldown / 1000.0f, 1).ToString();
                }
            }


            if (actions != null) {
                var listEnumerator2 = actions.GetEnumerator();
                for (var i = 0; listEnumerator2.MoveNext() == true; i++)
                {
                    Action currentItem = listEnumerator2.Current;
                    listView2.Items[i].SubItems[4].Text = Math.Round(currentItem.cooldown / 1000.0f, 1).ToString();
                }
            }


            if (serialPort1.IsOpen)
            {
                if (serialPort1.BytesToRead > 0)
                {
                    string returnMessage = serialPort1.ReadExisting();
                    if (returnMessage != "") Debug.Print(returnMessage);
                }
            }
            //string returnMessage = serialPort1.ReadExisting();

            //serialPort1.Write("{#ABC},");





        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            button1.Enabled = true;
            button2.Enabled = false;
        }


        Bitmap screenPixel = new Bitmap(1, 1, PixelFormat.Format32bppArgb);
        Bitmap screenRect = new Bitmap(300, 400, PixelFormat.Format32bppArgb);
        //Bitmap appRect = new Bitmap(WindowHook.WindowRectangle().Width, WindowHook.WindowRectangle().Height, PixelFormat.Format32bppArgb);

        public Color GetColorAt(Point location)
        {
            using (Graphics gdest = Graphics.FromImage(screenPixel))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, 1, 1, hSrcDC, location.X, location.Y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return screenPixel.GetPixel(0, 0);
        }
        public Bitmap GetRectAt(Point location)
        {
            using (Graphics gdest = Graphics.FromImage(screenRect))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, 300, 400, hSrcDC, location.X, location.Y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return screenRect;
        }

        public void RenderConditions()
        {
            listView1.Items.Clear();
            string[] arr = new string[6];
            ListViewItem itm;
            conditions.ForEach(delegate (Condition condition) {

            arr[0] = condition.comment;

            if (condition.enabled)
                arr[1] = condition.status;
            else arr[1] = "OFF";

            arr[2] = condition.cooldown.ToString();

            arr[3] = condition.condition;

            arr[4] = condition.parameter;



            if (condition.condition == "Color") {
                arr[5] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
            }
            else if (condition.condition == "ColorVertical") {
                arr[5] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
            }
            else if (condition.condition == "PixelCoordinates" || condition.condition == "PixelCoordinatesL2Ultra")
            {
                    arr[5] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
                    if (condition.point2 !=null)
                        arr[5] += ";"+condition.point2.ToString() + ColorTranslator.ToHtml(condition.color2);
            }
            else
            {
                arr[5] = condition.delay.ToString();
            }



            itm = new ListViewItem(arr);
            listView1.Items.Add(itm);
        });

            /*
            listView1.Columns.Add("Comment", 90);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);

            /*
            //Add column header
            listView1.Columns.Add("Condition", 65);
            listView1.Columns.Add("Parameter", 60);
            listView1.Columns.Add("Value", 140);
            listView1.Columns.Add("Status", 50);
            listView1.Columns.Add("Left", 40);
            listView1.Columns.Add("Comment", 90);
            */


            /*
            conditions.ForEach(delegate (Condition condition) {
                arr[0] = condition.condition;
                arr[1] = condition.parameter;
                if (condition.condition == "Color")
                {
                    arr[2] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
                }
                else if (condition.condition == "ColorVertical")
                {
                    arr[2] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
                }
                else if (condition.condition == "PixelCoordinates" || condition.condition == "PixelCoordinatesL2Ultra")
                {
                    arr[2] = condition.point.ToString() + ColorTranslator.ToHtml(condition.color);
                }
                else
                {
                    arr[2] = condition.delay.ToString();
                }

                if (condition.enabled)
                    arr[3] = condition.status;
                else arr[3] = "OFF";
                arr[4] = condition.cooldown.ToString();
                arr[5] = condition.comment;
                itm = new ListViewItem(arr);
                listView1.Items.Add(itm);
            });
            */
        }

        public void RenderActions()
        {
            listView2.Items.Clear();
            string[] arr = new string[6];
            ListViewItem itm;
            if (actions!=null)
            actions.ForEach(delegate (Action action) {
                arr[0] = action.action;
                if (action.action == "KeyPress")
                    arr[1] = action.parameter;
                else arr[1] = action.point.ToString();
                arr[2] = action.delay.ToString();
                arr[3] = "";
                arr[4] = action.cooldown.ToString();
                arr[5] = action.comment;
                itm = new ListViewItem(arr);
                listView2.Items.Add(itm);
            });
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Action _a = new Action();
            _a.action = comboBox7.Text;
            _a.parameter = comboBox6.Text;
            _a.point = tmpPoint;
            _a.delay = 0;
            _a.pause = checkBox1.Checked;
            _a.enabled = checkBox4.Checked;
            //_a.parentCondition = conditions[listView1.SelectedItems[0].Index];
            int j;
            if (Int32.TryParse(comboBox5.Text, out j))
                _a.delay = j;
            _a.comment = textBox2.Text;
            actions.Add(_a);
            currentAction = _a;
            RenderActions();
        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem item = listView2.SelectedItems.Count > 0 ? listView2.SelectedItems[0] : null;
            if (item != null)
            {
                comboBox7.Text = item.SubItems[0].Text;
                comboBox6.Text = item.SubItems[1].Text;
                comboBox5.Text = item.SubItems[2].Text;
                textBox2.Text = item.SubItems[5].Text;
                currentAction = actions[listView2.SelectedItems[0].Index];
                checkBox4.Checked = currentAction.enabled;
                comboBox12.Text = currentAction.freeze.ToString();
                tmpPoint = currentAction.point;
            }
            else
            {
                comboBox7.Text = "";
                comboBox6.Text = "";
                comboBox5.Text = "";
                textBox2.Text = "";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (currentAction != null)
                actions.Remove(currentAction);
            RenderActions();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            pickActionDelay = milliseconds + 3000;

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (currentAction != null)
            {
                currentAction.action = comboBox7.Text;
                currentAction.parameter = comboBox6.Text;
                currentAction.point = tmpPoint;
                currentAction.delay = 0;
                currentAction.pause = checkBox1.Checked;
                currentAction.enabled = checkBox4.Checked;

                int j;
                if (Int32.TryParse(comboBox12.Text, out j))
                    currentAction.freeze = j;

                if (Int32.TryParse(comboBox5.Text, out j))
                    currentAction.delay = j;
                currentAction.comment = textBox2.Text;
                RenderActions();
            }
        }


        private void SaveConditions(List<Condition> listofa)
        {
            string path = comboBox1.Text + ".sav";
            if (File.Exists(path))
            {
                var confirmResult = MessageBox.Show("File `"+ path + "` already exists.\nOverwrite with new one?",
                                     "Overwrite?",
                                     MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    FileStream outFile = File.Create(path);
                    XmlSerializer formatter = new XmlSerializer(listofa.GetType());
                    formatter.Serialize(outFile, listofa);
                    outFile.Close();
                }
                else
                {
                    // If 'No', do something here.
                }
            } else
            {
                FileStream outFile = File.Create(path);
                XmlSerializer formatter = new XmlSerializer(listofa.GetType());
                formatter.Serialize(outFile, listofa);
                outFile.Close();
            }


        }
        private List<Condition> LoadConditions()
        {
            string file = comboBox1.Text + ".sav";
            List<Condition> listofa = new List<Condition>();
            XmlSerializer formatter = new XmlSerializer(conditions.GetType());
            FileStream aFile = new FileStream(file, FileMode.Open);
            byte[] buffer = new byte[aFile.Length];
            aFile.Read(buffer, 0, (int)aFile.Length);
            MemoryStream stream = new MemoryStream(buffer);
            aFile.Close();
            GC.Collect();
            return (List<Condition>)formatter.Deserialize(stream);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(comboBox1.Text))
                SaveConditions(conditions);
            else
            {
                MessageBox.Show("Save name cannot be empty", "Please set Save name");
                comboBox1.Focus();
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            conditions = LoadConditions();

            conditions.ForEach(delegate (Condition condition)
            {
                Debug.Print(condition.dependId.ToString());
                if (condition.dependId != -1)
                {
                   
                    condition.depend = conditions[condition.dependId];
                }
            });

            actions = null;
            RenderConditions();
            RenderActions();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (currentCondition != null)
            {
                currentCondition.condition = comboBox2.Text;
                currentCondition.parameter = comboBox3.Text;

                tmpColor = Color.Empty;
                tmpColor2 = Color.Empty;
                tmpPoint = Point.Empty;
                tmpPoint2 = Point.Empty;

                char[] spearator0 = { ';' };
                String[] strlist0 = comboBox4.Text.Split(spearator0, 2, StringSplitOptions.None);

                int i = 0;
                foreach (var str in strlist0)
                {


                    char[] spearator1 = { '#' };
                    String[] strlist1 = str.Split(spearator1, 2, StringSplitOptions.None);

                    
                    
                    if (strlist1.Length > 1)
                    {
                        if (i == 0)
                            tmpColor = System.Drawing.ColorTranslator.FromHtml("#" + strlist1[1]);
                        if (i == 1)
                            tmpColor2 = System.Drawing.ColorTranslator.FromHtml("#" + strlist1[1]);


                        int startindex = comboBox4.Text.IndexOf('{');
                        int Endindex = comboBox4.Text.IndexOf('}');
                        string xy = comboBox4.Text.Substring(startindex + 1, Endindex - startindex - 1);
                        char[] spearator = { ',' };
                        String[] strlist = xy.Split(spearator, 2, StringSplitOptions.None);

                        if (i == 0)
                        {
                            tmpPoint.X = Int16.Parse(Regex.Replace(strlist[0], "[^0-9]", ""));
                            tmpPoint.Y = Int16.Parse(Regex.Replace(strlist[1], "[^0-9]", ""));
                        }
                        if (i == 1)
                        {
                            tmpPoint2.X = Int16.Parse(Regex.Replace(strlist[0], "[^0-9]", ""));
                            tmpPoint2.Y = Int16.Parse(Regex.Replace(strlist[1], "[^0-9]", ""));
                        }

                       
                    }
                    i++;

                }


                

                currentCondition.color = tmpColor;
                currentCondition.color2 = tmpColor2;
                currentCondition.point = tmpPoint;
                currentCondition.point2 = tmpPoint2;
                currentCondition.delay = 0;
                currentCondition.async = checkBox2.Checked;
                currentCondition.enabled = checkBox3.Checked;
                currentCondition.depend = tmpDepend;
                currentCondition.logic = comboBox9.Text == "TRUE";
                currentCondition.loopActions = loopActionsCheckBox.Checked;
                int j;
                if (Int32.TryParse(comboBox4.Text, out j))
                    currentCondition.delay = j;
                currentCondition.comment = textBox1.Text;
                RenderConditions();
            }
            else button3.PerformClick();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            pickConditionDelay = milliseconds + 3000;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Condition _c = new Condition();
            _c.id = conditions.Count;
            _c.condition = comboBox2.Text;
            _c.parameter = comboBox3.Text;
            _c.point = tmpPoint;
            _c.color = tmpColor;
            _c.delay = 0;
            _c.async = checkBox2.Checked;
            _c.enabled = checkBox3.Checked;
            _c.depend = tmpDepend;
            _c.logic = comboBox9.Text == "TRUE";
            int j;
            if (Int32.TryParse(comboBox4.Text, out j))
                _c.delay = j;
            _c.comment = textBox1.Text;
            conditions.Add(_c);
            currentCondition = _c;
            RenderConditions();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (currentCondition != null)
                conditions.Remove(currentCondition);
            RenderConditions();
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox8.SelectedIndex == 0)
                tmpDepend = null;
            else
                tmpDepend = conditions[comboBox8.SelectedIndex-1];
            if (tmpDepend!=null )Debug.Print(tmpDepend.ToString());
        }

        private void button9_Click(object sender, EventArgs e)
        {


             if (label18.Text == "Not connected")
             {
                 serialPort1.PortName = comboBox10.Text;
                 serialPort1.Open();
                 System.Threading.Thread.Sleep(100);
                 if (serialPort1.IsOpen)
                 {
                     label18.Text = "Connected";
                     Conditions.arduinoKeyboard = true;
                     button9.Text = "Disconnect";
                 }
             }
            else if (label18.Text=="Connected")
             {
                 serialPort1.Close();
                 label18.Text = "Not connected";
                 Conditions.arduinoKeyboard = false;
                 button9.Text = "Connect";
             }

        }

        private void button14_Click(object sender, EventArgs e)
        {
            WindowHook.sendKeystroke(comboBox11.Text, 'x');
            Console.WriteLine(WindowHook.WindowPosition().ToString());
        }

        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox11_MouseDown(object sender, MouseEventArgs e)
        {
            comboBox11.Items.Clear();
            foreach (Process process in WindowHook.FindWindows())
            {
                if (!String.IsNullOrEmpty(process.MainWindowTitle))
                    comboBox11.Items.Add(process.MainWindowTitle);
            }
        }

        private void ComboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (this.Width == 1527)
            {
                this.Width = 707;
                button15.Text = ">";
            }
            else
            {
                this.Width = 1527;
                button15.Text = "<";
            }
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }



        /*
        public static  Point ProcessBitmapFindNearestToCenterParallel(Bitmap processedBitmap, Color color)
        {
            unsafe
            {
                Bitmap _bitmap = processedBitmap;
                Point result = Point.Empty;
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                double minDistance = 99999;
                Point center = new Point(processedBitmap.Width / 2, processedBitmap.Height / 2);

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;




                        if (oldBlue == color.B && oldGreen == color.G && oldRed == color.R)
                        {
                            currentLine[x] = 0;
                            currentLine[x + 1] = 255;
                            currentLine[x + 2] = 255;

                            int realX = (x / (bytesPerPixel)) % bitmapData.Stride;
                            int realY = y;

                            double distance = GetDistance(realX, realY, center.X, center.Y);

                            //Debug.Print("d:"+distance.ToString());
                            if (distance < minDistance)
                            {
                                result = new Point(realX, realY + 40);
                                minDistance = distance;
                                Debug.Print("condition:" + result.ToString());
                            }
                        }


                    }
                });
                processedBitmap.UnlockBits(bitmapData);


                return result;
            }
        }
        */



        public static Point ProcessBitmapFindNearestToCenter(Bitmap processedBitmap, Bitmap writeBitmap, Color color, Color color2)
        {
            unsafe
            {
                Point result = Point.Empty;
                Point foundPoint = Point.Empty;
                double minDistance = 99999;
                Point center = new Point(processedBitmap.Width / 2, processedBitmap.Height / 2 - 20);

                int border = 80;
                int skipcenter = 180;

                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        // calculate new pixel value
                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;


                        if ((color!=Color.Empty && oldBlue == color.B && oldGreen == color.G && oldRed == color.R)
                            ||
                            (color2!=Color.Empty && oldBlue == color2.B && oldGreen == color2.G && oldRed == color2.R)
                           )
                        {
                            /*currentLine[x] = 0;
                            currentLine[x + 1] = 255;
                            currentLine[x + 2] = 255;*/

                            int realX = (x / (bytesPerPixel)) % bitmapData.Stride;
                            int realY = y;

                            double distance = GetDistance(realX, realY, center.X, center.Y);
                            if (distance < minDistance && distance > skipcenter)
                            {
                                //bounds:
                                if (realX > border && realX < bitmapData.Width - border && realY > border && realY < bitmapData.Height - border - 130)
                                {
                                    int realXadd = 6;
                                    result = new Point(realX + realXadd, realY + 60);
                                    foundPoint = new Point(realX, realY);
                                    minDistance = distance;
                                    Debug.Print("Target point:" + result.ToString());
                                } else
                                { //if pixel is out of bounds, just mark somewhere on the way to the point
                                    int wayX = realX;
                                    if (realX > processedBitmap.Width / 2) wayX = realX - processedBitmap.Width / 6;
                                    if (realX <= processedBitmap.Width / 2) wayX = realX + processedBitmap.Width / 6;
                                    int wayY = realY;
                                    if (realY > processedBitmap.Height / 2) wayY = realY - processedBitmap.Height / 6;
                                    if (realY <= processedBitmap.Height / 2) wayY = realY + processedBitmap.Height / 6;
                                    result = new Point(wayX, wayY);
                                    foundPoint = new Point(realX, realY);
                                    minDistance = distance;
                                }
                            }
                            }



                    }
                }
                processedBitmap.UnlockBits(bitmapData);

                    
                //mark final point and center
                using (Graphics gr = Graphics.FromImage(writeBitmap))
                {
                    Rectangle rect;
                    gr.SmoothingMode = SmoothingMode.AntiAlias;
                    rect = new Rectangle(result.X - 4, result.Y - 4, 8, 8);
                    gr.FillEllipse(Brushes.Red, rect);
                    using (Pen thick_pen = new Pen(Color.Lime, 2))
                    {
                        gr.DrawEllipse(thick_pen, rect);
                    }

                    rect = new Rectangle(foundPoint.X - 4, foundPoint.Y - 4, 8, 8);
                    gr.FillEllipse(Brushes.Yellow, rect);


                    rect = new Rectangle(center.X - 6, center.Y - 6, 12, 12);
                    gr.FillEllipse(Brushes.Red, rect);
                    using (Pen thick_pen = new Pen(Color.Blue, 2))
                    {
                        gr.DrawRectangle(thick_pen, rect);
                    }

                    rect = new Rectangle(center.X- skipcenter, center.Y- skipcenter, skipcenter*2, skipcenter*2);
                    //gr.FillEllipse(Brushes.Yellow, rect);
                    using (Pen thick_pen = new Pen(Color.Green, 2))
                    {
                        gr.DrawEllipse(thick_pen, rect);
                    }


                    rect = new Rectangle(border, border, bitmapData.Width - border*2, bitmapData.Height - border*2 - 100);
                    using (Pen thick_pen = new Pen(Color.Red, 1))
                    {
                        gr.DrawRectangle(thick_pen, rect);
                    }


                }

                Debug.Print("d:" + result.ToString());
                return result;
            }
        }




        public static Point ProcessBitmapFindDifference(Bitmap processedBitmap, Bitmap writeBitmap, Bitmap referenceBitmap)
        {
            unsafe
            {
                Point result = Point.Empty;


                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);
                BitmapData referenceData = referenceBitmap.LockBits(new Rectangle(0, 0, referenceBitmap.Width, referenceBitmap.Height), ImageLockMode.ReadWrite, referenceBitmap.PixelFormat);
                BitmapData writeData = writeBitmap.LockBits(new Rectangle(0, 0, writeBitmap.Width, writeBitmap.Height), ImageLockMode.ReadWrite, writeBitmap.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = referenceData.Height;
                int widthInBytes = referenceData.Width * bytesPerPixel;
                byte* ptrFirstPixelReference = (byte*)referenceData.Scan0;
                byte* ptrFirstPixelData = (byte*)bitmapData.Scan0;
                byte* ptrFirstPixelWrite = (byte*)writeData.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixelData + (y * bitmapData.Stride);
                    byte* referenceLine = ptrFirstPixelReference + (y * referenceData.Stride);
                    byte* writeLine = ptrFirstPixelWrite + (y * writeData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {

                        if (currentLine[x] != referenceLine[x])
                        {
                            referenceLine[x] = 0;
                            referenceLine[x + 1] = 0;
                            referenceLine[x + 2] = 255;
                        }
                    }
                }
                writeBitmap.UnlockBits(writeData);
                processedBitmap.UnlockBits(bitmapData);
                referenceBitmap.UnlockBits(referenceData);


                Debug.Print("d:" + result.ToString());
                return result;
            }
        }







        public static Point ProcessBitmapFindNearestToCenterL2Ultra(Bitmap processedBitmap, Bitmap writeBitmap)
        {
            unsafe
            {
                Color color1 = Color.FromArgb(169,228,156);
                Color color2 = Color.FromArgb(255,119,121);

                Point result = Point.Empty;
                double minDistance = 99999;
                Point center = new Point(processedBitmap.Width / 2, processedBitmap.Height / 2 - 100);

                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        // calculate new pixel value
                        currentLine[x] = (byte)oldBlue;
                        currentLine[x + 1] = (byte)oldGreen;
                        currentLine[x + 2] = (byte)oldRed;


                        if (
                            (oldBlue == color1.B && oldGreen == color1.G && oldRed == color1.R)
                            ||
                            (oldBlue == color2.B && oldGreen == color2.G && oldRed == color2.R)
                            )
                        {
                            /*currentLine[x] = 0;
                            currentLine[x + 1] = 255;
                            currentLine[x + 2] = 255;*/

                            int realX = (x / (bytesPerPixel)) % bitmapData.Stride;
                            int realY = y;

                            //bounds:
                            if (realX > 40 && realX < bitmapData.Width - 40 && realY > 40 && realY < bitmapData.Height - 100)
                            {
                                double distance = GetDistance(realX, realY, center.X, center.Y);
                                if (distance < minDistance)
                                {
                                    result = new Point(realX, realY + 40);
                                    minDistance = distance;
                                    Debug.Print("Target point:" + result.ToString());
                                }
                            }
                        }

                    }
                }
                processedBitmap.UnlockBits(bitmapData);


                //mark final point and center
                using (Graphics gr = Graphics.FromImage(writeBitmap))
                {
                    Rectangle rect;
                    gr.SmoothingMode = SmoothingMode.AntiAlias;
                    rect = new Rectangle(result.X - 4, result.Y - 4, 8, 8);
                    gr.FillEllipse(Brushes.Red, rect);
                    using (Pen thick_pen = new Pen(Color.Lime, 2))
                    {
                        gr.DrawEllipse(thick_pen, rect);
                    }
                    rect = new Rectangle(center.X - 6, center.Y - 6, 12, 12);
                    gr.FillEllipse(Brushes.Red, rect);
                    using (Pen thick_pen = new Pen(Color.Blue, 2))
                    {
                        gr.DrawRectangle(thick_pen, rect);
                    }
                }

                Debug.Print("d:" + result.ToString());
                return result;
            }
        }



        public Bitmap CaptureApplication(IntPtr hwnd)
        {

            var rect = new User32.Rect();
            User32.GetWindowRect(hwnd, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            
            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            
            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                graphics.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
            }

            return bmp;
        }
        private class User32
        {
            [StructLayout(LayoutKind.Sequential)]
            public struct Rect
            {
                public int left;
                public int top;
                public int right;
                public int bottom;
            }

            [DllImport("user32.dll")]
            public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
        }




        private void Test1Btn_Click(object sender, EventArgs e)
        {
            /*Stopwatch sw = new Stopwatch();


            sw.Restart();
            Bitmap image = CaptureApplication(WindowHook.HWND);
            sw.Stop();
            Console.WriteLine(string.Format("Captured in {0} ms.", sw.ElapsedMilliseconds));



            sw.Restart();
            Color findcolor = new Color();
            findcolor = Color.FromArgb(255, 251, 255);
            Point result = ProcessUsingLockbitsAndUnsafeAndParallel(image, findcolor);
            sw.Stop();
            Debug.WriteLine(image.Width.ToString() + "x" + image.Height.ToString());
            Console.WriteLine(string.Format("Processed in {0} ms.", sw.ElapsedMilliseconds));
            Console.WriteLine(string.Format("Result is {0} white pixels.", result.X));

            pictureBox1.Image = image;*/

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {

        }
    }






    public class WindowHook
    {
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr point);

        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hwnd, out Rectangle lpRect);

        public static IntPtr HWND = new IntPtr();


        public static Point Cursor()
        {
            Point cursor = new Point();
            GetCursorPos(ref cursor);
            return new Point(cursor.X - WindowPosition().X, cursor.Y - WindowPosition().Y);
        }

        public static Point Screen(Point point)
        {
            return new Point(point.X + WindowPosition().X, point.Y + WindowPosition().Y);
        }

        public static Point WindowPosition()
        {
            Rectangle rect;
            GetWindowRect(HWND, out rect);
            return new Point(rect.X,rect.Y);
        }

        public static Rectangle WindowRectangle()
        {
            Rectangle rect;
            GetWindowRect(HWND, out rect);
            return rect;
        }

        public static void sendKeystroke(string window, ushort k)
        {
            const uint WM_KEYDOWN = 0x100;
            const uint WM_KEYUP = 0x101;
            const uint WM_SYSCOMMAND = 0x018;
            const uint SC_CLOSE = 0x053;


            IntPtr WindowToFind = FindWindow(null, window);
            HWND = WindowToFind;
            SetForegroundWindow(WindowToFind);
            //SendKeys.SendWait("test");
            //IntPtr result3 = PostMessage(WindowToFind, WM_KEYDOWN, ((IntPtr)k), (IntPtr)0);
            //IntPtr result4 = SendMessage(WindowToFind, WM_KEYUP, ((IntPtr)k), (IntPtr)0);
        }

        public static Process[] FindWindows()
        {
            Process[] processlist = Process.GetProcesses();
            
            /*foreach (Process process in processlist)
            {
                if (!String.IsNullOrEmpty(process.MainWindowTitle))
                {
                        Console.WriteLine("Process: {0} ID: {1} Window title: {2}", process.ProcessName, process.Id, process.MainWindowTitle);
                }
            }*/

            return processlist;
        }

    }


    public static class Conditions
    {
        public static List<Condition> conditions { get; set; } = new List<Condition>();
        public static List<String> toCom { get; set; } = new List<String>();
        public static bool arduinoKeyboard { get; set; } = new bool();
    }


    /*
     * C O N D I T I O N S
    */
    public class Condition
    {

        [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int BitBlt(IntPtr hDC, int x, int y, int nWidth, int nHeight, IntPtr hSrcDC, int xSrc, int ySrc, int dwRop);

        public int id;
        public string condition;
        public string parameter;
        public string value;
        public string status;
        public string comment;
        public int delay;
        private long curDelay;
        public long cooldown;
        private long renderCurDelay;
        private long renderCooldown;
        public Point point;
        public Point point2;
        public Point _p;
        public bool async = true;
        public bool enabled = true;
        public bool logic = true;
        private Bitmap bmp;
        //private AdvancedOcr Ocr;
        //private AutoOcr Ocr;
        private Point returnPoint;
        private int curAction = 0;
        public bool loopActions = false;

        private Condition _depend;
        [XmlIgnore]
        public Condition depend
        {
            get { return this._depend; }
            set { _depend = value;  if (value != null) this.dependId = value.id; else this.dependId = -1; }
        }
        [XmlElement("depend")]
        public int dependId;

        [XmlIgnore]
        public Color color;
        [XmlElement("color")]
        public int ColorAsArgb
        {
            get { return color.ToArgb(); }
            set { color = Color.FromArgb(value); }
        }

        [XmlIgnore]
        public Color color2;
        [XmlElement("color2")]
        public int ColorAsArgb2
        {
            get { return color2.ToArgb(); }
            set { color2 = Color.FromArgb(value); }
        }






        public List<Action> actions = new List<Action>();

        [XmlIgnore]
        public Bitmap Bmp
        {
            get { return bmp; }
            set { bmp = value; }
        }

        [XmlIgnore]
        public Point ReturnPoint
        {
            get { return returnPoint; }
            set { returnPoint = value; }
        }


        public bool CursorOverWindow()
        {
            Rectangle rect = WindowHook.WindowRectangle();
            return (Cursor.Position.X > rect.X && Cursor.Position.X < rect.Width && Cursor.Position.Y > rect.Y && Cursor.Position.Y < rect.Height);

        }

        public Condition()
        {

            //Ocr = new AutoOcr();
        /*
            Ocr = new AdvancedOcr()
            {
                //CleanBackgroundNoise = true,
                //EnhanceContrast = true,
                EnhanceResolution = true,
                //Language = IronOcr.Languages.English.OcrLanguagePack,
                Strategy = IronOcr.AdvancedOcr.OcrStrategy.Advanced,
                ColorSpace = AdvancedOcr.OcrColorSpace.Color,
                DetectWhiteTextOnDarkBackgrounds = true,
                InputImageType = AdvancedOcr.InputTypes.Snippet,
                //RotateAndStraighten = true,
                ReadBarCodes = false
                //ColorDepth = 16
            };
            */
        }
       


        public void Update()
        {
            if (!enabled) return;

            if (depend!=null && (depend.status == "TRUE")!=logic )
            {
                status = "FALSE*";
                return;
            }

            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            cooldown = curDelay - milliseconds;
            renderCooldown = renderCurDelay - milliseconds;
            status = "FALSE";
            
            if (cooldown <= 0)
            {
                if (condition == "Color")
                {
                    Point _point = WindowHook.Screen(point);
                    curDelay = milliseconds + delay;
                    if (parameter == "Equal =" && GetColorAt(_point) == color)
                    {
                        status = "TRUE";
                    }

                    if (parameter == "Not <>" && GetColorAt(_point) != color)
                    {
                        status = "TRUE";
                    }
                }
                else if (condition == "Every" && milliseconds > curDelay)
                {
                    status = "TRUE";
                    curDelay = milliseconds + delay;
                }
                else if (condition == "ColorVertical")
                {

                    Point _point = WindowHook.Screen(point);
                    Bitmap _bmp = GetRectAt(_point);
                    bool found = false;
                    for (int y = 0; y < _bmp.Height; y++)
                    {
                        for (int x = 0; x < _bmp.Width; x++)
                        {
                            Color pixelColor = _bmp.GetPixel(x, y);
                            if (pixelColor == color)
                            {
                                _bmp.SetPixel(x, y, Color.FromArgb(255,0,0));
                                found = true;
                            }
                        }
                    }

                    if (parameter == "Equal =" && found)
                    {
                        status = "TRUE";
                    }

                    if (parameter == "Not <>" && !found)
                    {
                        status = "TRUE";
                    }
                }



                else if (condition == "PixelCoordinates")
                {
                    if (CursorOverWindow())
                    {
                        //Bitmap _bitmap = new Bitmap(Form1.ApplicationBitmap);
                        //Bitmap _bitmap = (Bitmap) Form1.ApplicationBitmap.Clone();
                        this.returnPoint = Form1.ProcessBitmapFindNearestToCenter(Form1.ApplicationBitmap, Form1.ApplicationBitmap2, color,color2);

                        //this.returnPoint = Form1.ProcessBitmapFindNearestToCenter(Form1.ApplicationBitmap, color);
                        //this.returnPoint = Form1.ProcessBitmapFindNearestToCenter(_bitmap, color);
                        //_bitmap.Dispose();

                        if (this.returnPoint != Point.Empty)
                        {
                            actions.ForEach(delegate (Action action)
                            {
                                if (action.action == "MoveMouseLClick")
                                {
                                    action.point.X = this.returnPoint.X + WindowHook.WindowPosition().X;
                                    action.point.Y = this.returnPoint.Y + WindowHook.WindowPosition().Y;
                                }
                            });
                        }

                        //Debug.Print("returnPoint: " + this.returnPoint.ToString());
                        if (parameter == "Equal =" && this.returnPoint != Point.Empty)
                        {
                            status = "TRUE";
                        }

                        if (parameter == "Not <>" && this.returnPoint == Point.Empty)
                        {
                            status = "TRUE";
                        }
                    }

                }


                else if (condition == "PixelDifference")
                {
                    if (CursorOverWindow() && Form1.ReferenceApplicationBitmap!=null)
                    {
                        this.returnPoint = Form1.ProcessBitmapFindDifference(Form1.ApplicationBitmap, Form1.ApplicationBitmap2, Form1.ReferenceApplicationBitmap);
                        /*if (this.returnPoint != Point.Empty)
                        {
                            actions.ForEach(delegate (Action action)
                            {
                                if (action.action == "MoveMouseLClick")
                                {
                                    action.point.X = this.returnPoint.X + WindowHook.WindowPosition().X;
                                    action.point.Y = this.returnPoint.Y + WindowHook.WindowPosition().Y;
                                }
                            });
                        }*/

                        //Debug.Print("returnPoint: " + this.returnPoint.ToString());
                        if (parameter == "Equal =" && this.returnPoint != Point.Empty)
                        {
                            status = "TRUE";
                        }

                        if (parameter == "Not <>" && this.returnPoint == Point.Empty)
                        {
                            status = "TRUE";
                        }
                    }

                }

                else if (condition == "PixelCoordinatesL2Ultra")
                {
                    if (CursorOverWindow())
                    {
                        //Bitmap _bitmap = new Bitmap(Form1.ApplicationBitmap);
                        //Bitmap _bitmap = (Bitmap) Form1.ApplicationBitmap.Clone();
                        this.returnPoint = Form1.ProcessBitmapFindNearestToCenterL2Ultra(Form1.ApplicationBitmap, Form1.ApplicationBitmap2);

                        //this.returnPoint = Form1.ProcessBitmapFindNearestToCenter(Form1.ApplicationBitmap, color);
                        //this.returnPoint = Form1.ProcessBitmapFindNearestToCenter(_bitmap, color);
                        //_bitmap.Dispose();

                        if (this.returnPoint != Point.Empty)
                        {
                            actions.ForEach(delegate (Action action)
                            {
                                if (action.action == "MoveMouseLClick")
                                {
                                    action.point.X = this.returnPoint.X + WindowHook.WindowPosition().X;
                                    action.point.Y = this.returnPoint.Y + WindowHook.WindowPosition().Y;
                                }
                            });
                        }

                        //Debug.Print("returnPoint: " + this.returnPoint.ToString());
                        if (parameter == "Equal =" && this.returnPoint != Point.Empty)
                        {
                            status = "TRUE";
                        }

                        if (parameter == "Not <>" && this.returnPoint == Point.Empty)
                        {
                            status = "TRUE";
                        }
                    }

                }
                else if (condition == "MouseOutside")
                {
                    this.returnPoint = Point.Empty;
                    if (!CursorOverWindow())
                    {
                        int width = WindowHook.WindowRectangle().Width - WindowHook.WindowRectangle().Left;
                        int height = WindowHook.WindowRectangle().Height - WindowHook.WindowRectangle().Top;
                        int centerX = WindowHook.WindowPosition().X + width / 2;
                        int centerY = WindowHook.WindowPosition().Y + height / 2;

                        Point centerPoint = new Point(centerX, centerY);
                        this.returnPoint = centerPoint;
                        //Console.WriteLine(centerPoint.ToString());


                        if (this.returnPoint != Point.Empty)
                        {
                            actions.ForEach(delegate (Action action)
                            {
                                if (action.action == "MoveMouseLClick")
                                {
                                    action.point.X = this.returnPoint.X;
                                    action.point.Y = this.returnPoint.Y;
                                }
                            });
                        }

                        //Debug.Print("returnPoint: " + this.returnPoint.ToString());
                        if (parameter == "Equal =" && this.returnPoint != Point.Empty)
                        {
                            status = "TRUE";
                        }

                        if (parameter == "Not <>" && this.returnPoint == Point.Empty)
                        {
                            status = "TRUE";
                        }
                    }

                }

                else if (condition == "CursorOverWindow")
                {
                        if (parameter == "Equal =" && CursorOverWindow())
                        {
                            status = "TRUE";
                        }

                        if (parameter == "Not <>" && !CursorOverWindow())
                        {
                            status = "TRUE";
                        }
                }





                /*else if (condition == "Text")
                {
                    //var Ocr = new IronOcr.AutoOcr();
                    var Result = Ocr.Read(@"sample.jpg");
                    Console.WriteLine(Result.Text);
                    curDelay = milliseconds + delay;
                }*/
            }
        }

        public void Process()
        {
            if (!enabled) return;
            Update();

            if (loopActions)
            {
                if (status == "TRUE")
                {
                    actions.ElementAt(curAction).Process();
                    curAction++;
                    if (curAction >= actions.Count) curAction = 0;
                }
            }
            else
            {
                actions.ForEach(delegate (Action action)
                {
                    if (status == "TRUE")
                    {
                        action.Process();
                    }
                    //Update();
                });
            }
            
        }

        public static double GetDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt(Math.Pow((x2 - x1), 2) + Math.Pow((y2 - y1), 2));
        }

        Bitmap screenPixel = new Bitmap(1, 1, PixelFormat.Format32bppArgb);
        Bitmap screenRect = new Bitmap(300, 400, PixelFormat.Format32bppArgb);
        Bitmap appRect = new Bitmap(WindowHook.WindowRectangle().Width - WindowHook.WindowRectangle().X, WindowHook.WindowRectangle().Height - WindowHook.WindowRectangle().Y, PixelFormat.Format32bppArgb);


        public Color GetColorAt(Point location)
        {
            using (Graphics gdest = Graphics.FromImage(screenPixel))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, 1, 1, hSrcDC, location.X, location.Y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return screenPixel.GetPixel(0, 0);
        }


        public Bitmap GetRectAt(Point location)
        {
            using (Graphics gdest = Graphics.FromImage(screenRect))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, 300, 400, hSrcDC, location.X, location.Y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return screenRect;
        }

        public Bitmap GetAppRect(Point location)
        {
            using (Graphics gdest = Graphics.FromImage(appRect))
            {
                using (Graphics gsrc = Graphics.FromHwnd(IntPtr.Zero))
                {
                    IntPtr hSrcDC = gsrc.GetHdc();
                    IntPtr hDC = gdest.GetHdc();
                    int retval = BitBlt(hDC, 0, 0, appRect.Width, appRect.Height, hSrcDC, location.X, location.Y, (int)CopyPixelOperation.SourceCopy);
                    gdest.ReleaseHdc();
                    gsrc.ReleaseHdc();
                }
            }

            return appRect;
        }


        public void Log()
        {
            Debug.Print(ToString());
        }
        public override string ToString()
        {
            return condition + " " + delay.ToString() + " " + parameter + " " + comment;
        }

    }







    /*
     * A C T I O N S
    */



    public class Action
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);
        //Mouse actions
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;

        [DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);
        [DllImport("user32.dll")]
        static extern bool GetCursorPos(ref Point lpPoint);

        public string action;
        public string parameter;
        public int delay;
        private long curDelay;
        public int status;
        public Point point;
        public long cooldown;
        public string comment;
        public bool pause = false;
        public long freeze = 0;
        public long curFreeze = 0;
        public long freezeOldMilliseconds;
        public bool enabled = true;

        //[XmlIgnore]
        //public Condition parentCondition;




        public bool CursorOverWindow()
        {
            Rectangle rect = WindowHook.WindowRectangle();
            return (Cursor.Position.X > rect.X && Cursor.Position.X < rect.Width && Cursor.Position.Y > rect.Y && Cursor.Position.Y < rect.Height);

        }





        public void Process()
        {
            if (!enabled) return;
            long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            cooldown = curDelay - milliseconds;

            curFreeze = freeze;
            freezeOldMilliseconds = milliseconds;

            if (milliseconds > curDelay)
            {
                Point _point = WindowHook.Screen(point);
                curDelay = milliseconds + delay;

                if (action == "KeyPress")
                {
                    if (Conditions.arduinoKeyboard)
                        Conditions.toCom.Add(parameter);
                    else
                        SendKeys.Send(parameter);
                }
                if (action == "LeftClick")
                {
                    if (Conditions.arduinoKeyboard)
                        Conditions.toCom.Add("{MOUSE_LEFT}");
                    else
                    {
                        Point oldpos = Cursor.Position;
                        SetCursorPos(_point.X, _point.Y);
                        mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(30);
                        mouse_event(MOUSEEVENTF_LEFTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(100);
                        SetCursorPos(oldpos.X, oldpos.Y);
                    }
                }
                if (action == "RightClick")
                {
                    if (Conditions.arduinoKeyboard)
                        Conditions.toCom.Add("{MOUSE_RIGHT}");
                    else
                    {
                        Point oldpos = Cursor.Position;
                        SetCursorPos(_point.X, _point.Y);
                        mouse_event(MOUSEEVENTF_RIGHTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(30);
                        mouse_event(MOUSEEVENTF_RIGHTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(100);
                        SetCursorPos(oldpos.X, oldpos.Y);
                    }
                }

                if (action == "SwipeRLeft")
                {
                    if (Conditions.arduinoKeyboard)
                    {
                        if (CursorOverWindow())
                        {
                            Conditions.toCom.Add("{SWIPE_R_LEFT}");
                        }
                    }
                    else
                    {
                        Point oldpos = Cursor.Position;
                        SetCursorPos(_point.X, _point.Y);
                        mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(200);
                        SetCursorPos(_point.X - 50, _point.Y);
                        System.Threading.Thread.Sleep(200);
                        mouse_event(MOUSEEVENTF_LEFTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        System.Threading.Thread.Sleep(100);
                        SetCursorPos(oldpos.X, oldpos.Y);
                    }
                }


                if (action == "MoveMouseLClick")
                {
                    //using point from action, not from screen
                    /*if (parentCondition.ReturnPoint != Point.Empty)
                        point = parentCondition.ReturnPoint;

                    Debug.Print("action: "+parentCondition.ReturnPoint.ToString());*/

                    _point = point;
                    if (Conditions.arduinoKeyboard)
                    {

                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        vector.X = (_point.X - oldpos.X) / 2;
                        vector.Y = (_point.Y - oldpos.Y) / 2;

                        if (vector.X > 80)
                            vector.X = 80;
                        if (vector.X < -80)
                            vector.X = -80;

                        if (vector.Y > 80)
                            vector.Y = 80;
                        if (vector.Y < -80)
                            vector.Y = -80;

                        Conditions.toCom.Add("{MOUSE_MOVE}" + vector.X.ToString() + "," + vector.Y.ToString() + ",0");

                        if (Math.Round((float)oldpos.X/50.0f) == Math.Round((float)_point.X/50.0f) && Math.Round((float)oldpos.Y/50.0f) == Math.Round((float)_point.Y/50.0f))
                        {
                            Conditions.toCom.Add("{MOUSE_LEFT}");
                            //Debug.Print("MATCH!!!");
                        }

                    }
                    else
                    {
                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        vector.X = (int)Math.Round((_point.X - oldpos.X) * 0.5f);
                        vector.Y = (int)Math.Round((_point.Y - oldpos.Y) * 0.5f);

                        if (Math.Abs(_point.X - oldpos.X) < 30)
                            vector.X = _point.X - oldpos.X;
                        if (Math.Abs(_point.Y - oldpos.Y) < 30)
                            vector.Y = _point.Y - oldpos.Y;

                        SetCursorPos(oldpos.X + vector.X, oldpos.Y + vector.Y);

                        if (Math.Round((float)oldpos.X / 50.0f) == Math.Round((float)_point.X / 50.0f) && Math.Round((float)oldpos.Y / 50.0f) == Math.Round((float)_point.Y / 50.0f))
                        {
                            mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                            System.Threading.Thread.Sleep(20);
                            mouse_event(MOUSEEVENTF_LEFTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        }

                    }
                }

                if (action == "Sound")
                {
                    System.Media.SystemSounds.Beep.Play();
                }




                if (action == "LeftClickAtPoint")
                {

                    _point.X = point.X + WindowHook.WindowPosition().X;
                    _point.Y = point.Y + WindowHook.WindowPosition().Y;

                    if (Conditions.arduinoKeyboard)
                    {

                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        Debug.Print("point");
                        Debug.Print(_point.ToString());
                        Debug.Print("oldpos");
                        Debug.Print(oldpos.ToString());

                        vector.X = (_point.X - oldpos.X) / 2;
                        vector.Y = (_point.Y - oldpos.Y) / 2;

                        if (vector.X > 80)
                            vector.X = 80;
                        if (vector.X < -80)
                            vector.X = -80;

                        if (vector.Y > 80)
                            vector.Y = 80;
                        if (vector.Y < -80)
                            vector.Y = -80;

                        Conditions.toCom.Add("{MOUSE_MOVE}" + vector.X.ToString() + "," + vector.Y.ToString() + ",0");

                        if (Math.Round((float)oldpos.X / 10.0f) == Math.Round((float)_point.X / 10.0f) && Math.Round((float)oldpos.Y / 10.0f) == Math.Round((float)_point.Y / 10.0f))
                        {
                            Conditions.toCom.Add("{MOUSE_LEFT}");
                            //Debug.Print("MATCH!!!");
                        }

                    }
                    else
                    {
                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        vector.X = (int)Math.Round((_point.X - oldpos.X) * 0.5f);
                        vector.Y = (int)Math.Round((_point.Y - oldpos.Y) * 0.5f);

                        if (Math.Abs(_point.X - oldpos.X) < 30)
                            vector.X = _point.X - oldpos.X;
                        if (Math.Abs(_point.Y - oldpos.Y) < 30)
                            vector.Y = _point.Y - oldpos.Y;

                        SetCursorPos(oldpos.X + vector.X, oldpos.Y + vector.Y);

                        if (Math.Round((float)oldpos.X / 10.0f) == Math.Round((float)_point.X / 10.0f) && Math.Round((float)oldpos.Y / 10.0f) == Math.Round((float)_point.Y / 10.0f))
                        {
                            mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                            System.Threading.Thread.Sleep(20);
                            mouse_event(MOUSEEVENTF_LEFTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        }

                    }
                }


                if (action == "RightClickAtPoint")
                {

                    _point.X = point.X + WindowHook.WindowPosition().X;
                    _point.Y = point.Y + WindowHook.WindowPosition().Y;

                    if (Conditions.arduinoKeyboard)
                    {

                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        Debug.Print("point");
                        Debug.Print(_point.ToString());
                        Debug.Print("oldpos");
                        Debug.Print(oldpos.ToString());

                        vector.X = (_point.X - oldpos.X) / 2;
                        vector.Y = (_point.Y - oldpos.Y) / 2;

                        if (vector.X > 80)
                            vector.X = 80;
                        if (vector.X < -80)
                            vector.X = -80;

                        if (vector.Y > 80)
                            vector.Y = 80;
                        if (vector.Y < -80)
                            vector.Y = -80;

                        Conditions.toCom.Add("{MOUSE_MOVE}" + vector.X.ToString() + "," + vector.Y.ToString() + ",0");

                        if (Math.Round((float)oldpos.X / 10.0f) == Math.Round((float)_point.X / 10.0f) && Math.Round((float)oldpos.Y / 10.0f) == Math.Round((float)_point.Y / 10.0f))
                        {
                            Conditions.toCom.Add("{MOUSE_RIGHT}");
                            //Debug.Print("MATCH!!!");
                        }

                    }
                    else
                    {
                        Point oldpos = Cursor.Position;
                        Point vector = new Point(0, 0);

                        vector.X = (int)Math.Round((_point.X - oldpos.X) * 0.5f);
                        vector.Y = (int)Math.Round((_point.Y - oldpos.Y) * 0.5f);

                        if (Math.Abs(_point.X - oldpos.X) < 30)
                            vector.X = _point.X - oldpos.X;
                        if (Math.Abs(_point.Y - oldpos.Y) < 30)
                            vector.Y = _point.Y - oldpos.Y;

                        SetCursorPos(oldpos.X + vector.X, oldpos.Y + vector.Y);

                        if (Math.Round((float)oldpos.X / 10.0f) == Math.Round((float)_point.X / 10.0f) && Math.Round((float)oldpos.Y / 10.0f) == Math.Round((float)_point.Y / 10.0f))
                        {
                            mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)_point.X, (uint)_point.Y, 0, 0);
                            System.Threading.Thread.Sleep(20);
                            mouse_event(MOUSEEVENTF_LEFTUP, (uint)_point.X, (uint)_point.Y, 0, 0);
                        }

                    }
                }





                Log();

                //Form1.freeze = freeze;

            }
            
        }

        public void Setup()
        {
            action = "LeftClick";
            point = new Point(200, 200);
            delay = 1000;
            Random rnd = new Random();
            comment  = "comment #" + rnd.Next(10, 99).ToString() + " " + DateTime.Now.Ticks;
        }

        public void Log()
        {
            Debug.Print(ToString());
        }
        public override string ToString()
        {
            return action + " " + delay.ToString()+ " " + parameter + " " + comment;
        }

    }




}
